# Packing List

☐ T-Shirts
☐ Shirt
☐ Jumper
☐ Body Warmer
☐ Raincoat
☐ Underware
☐ Socks
☐ Swim Shorts
☐ Gloves
☐ Hat
☐ Belt

---

☐ Running Shoes
☐ Running Shorts
☐ Running Top
☐ Running Phone Arm Band

---

☐ Ear Plugs
☐ Eye mask
☐ Travel Pillow
☐ Toothbrush
☐ Razor
☐ Shampoo
☐ Deodrant
☐ Toothpaste
☐ Moisturiser
☐ Hair Products

---

☐ Pen
☐ Notepad
☐ Business cards

---

☐ Euros/Dollars/Krona/etc.
☐ Business Debit
☐ Personal Debit
☐ Personal Credit
☐ Monzo
☐ Driving License
☐ Wallet
☐ Call banks regarding international travel

---

☐ EHIC Card
☐ OZCaart
☐ Charliecard
☐ Travel Insurance Info.

---

☐ Kindle
☐ Book
☐ Sunglasses

---

☐ Passport
☐ Visa
☐ Flight tickets
☐ Train tickets

---

☐ Phone
☐ Headphones
☐ Laptop
☐ Laptop Charger
☐ Adapter Plug
☐ Phone Charger Cable
☐ Presentation Clicker
☐ HDMI Cable
☐ Microphone
☐ Projector

---

☐ Water
☐ Gifts (chocolate!)
☐ Decaf tea bags